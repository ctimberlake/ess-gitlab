# This file is a template, and might need editing before it works on your project.
FROM python:3.9

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt && apt-get update -y && apt-get install curl jq -y

COPY . /usr/src/app
RUN chmod +X /usr/src/app/check-pipeline.sh && chmod 777 /usr/src/app/check-pipeline.sh

# For some other command
CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way."]
