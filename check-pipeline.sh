#!/bin/bash
gitlabFile="$PWD/$1"
badWords="echo print printf cat ls"
badWordCount=0


check_file() {
    echo "Checking for bad word... $1"
    if grep -w $1 $gitlabFile
    then
        echo "Bad Word Found: $1"
        badWordCount=$(( $badWordCount + 1 ))
    fi
}


echo "Checking $gitlabFile ..."
for val in $badWords; do
    check_file $val
done

if (( badWordCount > 0 ));
then
	exit 1
else
	echo "No Bad Words Found! A-OK!"
fi
